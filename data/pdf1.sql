	DROP DATABASE IF EXISTS pdf1;

	CREATE DATABASE pdf1
	DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
	 
	USE pdf1;

	-- tablas
	DROP TABLE IF EXISTS imagen;
	CREATE TABLE imagen(
		img_n INT(9) AUTO_INCREMENT PRIMARY KEY,
		link VARCHAR(200) NOT NULL, /* dirección donde se ve la imagen y no la web */ 
		descripcion VARCHAR(200)
		);


	DROP TABLE IF EXISTS pregunta;
	CREATE TABLE pregunta(
		pre_n INT (9) AUTO_INCREMENT PRIMARY KEY,
		cuestion TEXT NULL, /*Enunciado de la pregunta*/
		img_n INT(9)
		);
		

	DROP TABLE IF EXISTS categoria;
	CREATE TABLE categoria(
		cat_id int (9)  AUTO_INCREMENT PRIMARY KEY,
		cat_titulo varchar(40) NULL, /* nombre o tipo de la categoria de la pregunta individual*/ 
		img_n INT(9),
		pertenece int (9)
	);


	DROP TABLE IF EXISTS respuesta;
	CREATE TABLE respuesta(
		res_n INT(9) AUTO_INCREMENT PRIMARY KEY, /* número de respuesta */
		letra CHAR(1) NOT NULL, /* posibilidad de una respuesta pudiendo ser a,b,c,d.. */
		pre_n INT(9) NOT NULL, /* número de la pregunta para esta respuesta */ 
		txtres TEXT NOT NULL,  /* redacción de la respuesta */
		correcta BOOLEAN NOT NULL, /* con 0 indica que es falsa y con 1 es verdadera */ 
		img_n INT(9),
		UNIQUE KEY (letra,pre_n)
		);
		
		

	DROP TABLE IF EXISTS nombretest;
	CREATE TABLE nombretest(
		id_n  INT(9) AUTO_INCREMENT PRIMARY KEY,
		titulo VARCHAR (55),
		fecha date,
		tipo_test VARCHAR (55),
		img_n INT(9)
		);


	DROP TABLE IF EXISTS test;
	CREATE TABLE test(
		id_t  INT(9) AUTO_INCREMENT PRIMARY KEY,
		id_n  INT(9), /* título del text de la tablas de nombrestest */
		pre_n INT(9) /*número de pregunta que lo compone*/
		);

	DROP TABLE IF EXISTS categoriaspregunta;
	CREATE TABLE categoriaspregunta(
		id_cp  INT(9) AUTO_INCREMENT PRIMARY KEY,
		pre_n  INT(9), 
		cat_id INT(9)
		);

	DROP TABLE IF EXISTS categoriastest;
	CREATE TABLE categoriastest(
		id_ct  INT(9) AUTO_INCREMENT PRIMARY KEY,
		id_n  INT(9), 
		cat_id INT(9)
		);




	-- añadir más restricciones a las tablas

	ALTER TABLE categoria
	ADD CONSTRAINT fk_categoria_imagen
	FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`),
	ADD CONSTRAINT fk_categoria_pertenece
	FOREIGN KEY (`pertenece`) REFERENCES categoria(`cat_id`);


	ALTER TABLE respuesta
	ADD CONSTRAINT fk_respuesta_pregunta
	FOREIGN KEY (`pre_n`) REFERENCES pregunta(`pre_n`),
	ADD CONSTRAINT fk_respuesta_imagen
	FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`);

	ALTER TABLE pregunta
	ADD CONSTRAINT fk_pregunta_imagen
	FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`);


	ALTER TABLE nombretest
	ADD CONSTRAINT fk_nombretest_imagen
	FOREIGN KEY (`img_n`) REFERENCES imagen(`img_n`);


	ALTER TABLE test
	ADD CONSTRAINT fk_test_pregunta
	FOREIGN KEY (`pre_n`)REFERENCES pregunta(`pre_n`),
	ADD CONSTRAINT fk_test_nombretest
	FOREIGN KEY (`id_n`) REFERENCES `nombretest`(`id_n`);
	

	
	ALTER TABLE categoriaspregunta
	ADD CONSTRAINT fk_categoriaspregunta_pregunta
	FOREIGN KEY (`pre_n`)REFERENCES pregunta(`pre_n`),
	ADD CONSTRAINT fk_categoriaspregunta_categoria
	FOREIGN KEY (`cat_id`)REFERENCES categoria(`cat_id`);

	ALTER TABLE categoriastest
	ADD CONSTRAINT fk_categoriastest_nombretest
	FOREIGN KEY (`id_n`)REFERENCES nombretest(`id_n`),
	ADD CONSTRAINT fk_categoriastest_categoria
	FOREIGN KEY (`cat_id`)REFERENCES categoria(`cat_id`);
