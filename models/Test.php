<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property int $id_t
 * @property int|null $id_n
 * @property int|null $pre_n
 *
 * @property Nombretest $n
 * @property Pregunta $preN
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_n', 'pre_n'], 'integer'],
            [['id_n'], 'exist', 'skipOnError' => true, 'targetClass' => Nombretest::className(), 'targetAttribute' => ['id_n' => 'id_n']],
            [['pre_n'], 'exist', 'skipOnError' => true, 'targetClass' => Pregunta::className(), 'targetAttribute' => ['pre_n' => 'pre_n']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_t' => 'Id T',
            'id_n' => 'Id N',
            'pre_n' => 'Pre N',
        ];
    }

    /**
     * Gets query for [[N]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getN()
    {
        return $this->hasOne(Nombretest::className(), ['id_n' => 'id_n']);
    }

    /**
     * Gets query for [[PreN]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreN()
    {
        return $this->hasOne(Pregunta::className(), ['pre_n' => 'pre_n']);
    }
}
