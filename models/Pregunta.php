<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pregunta".
 *
 * @property int $pre_n
 * @property string|null $cuestion
 * @property int|null $img_n
 *
 * @property Categoriaspregunta[] $categoriaspreguntas
 * @property Imagen $imgN
 * @property Respuesta[] $respuestas
 * @property Test[] $tests
 */
class Pregunta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pregunta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cuestion'], 'string'],
            [['img_n'], 'integer'],
            [['img_n'], 'exist', 'skipOnError' => true, 'targetClass' => Imagen::className(), 'targetAttribute' => ['img_n' => 'img_n']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pre_n' => 'Pre N',
            'cuestion' => 'Cuestion',
            'img_n' => 'Img N',
        ];
    }

    /**
     * Gets query for [[Categoriaspreguntas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriaspreguntas()
    {
        return $this->hasMany(Categoriaspregunta::className(), ['pre_n' => 'pre_n']);
    }

    /**
     * Gets query for [[ImgN]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImgN()
    {
        return $this->hasOne(Imagen::className(), ['img_n' => 'img_n']);
    }

    /**
     * Gets query for [[Respuestas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRespuestas()
    {
        return $this->hasMany(Respuesta::className(), ['pre_n' => 'pre_n']);
    }

    /**
     * Gets query for [[Tests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTests()
    {
        return $this->hasMany(Test::className(), ['pre_n' => 'pre_n']);
    }
}
