<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nombretest".
 *
 * @property int $id_n
 * @property string|null $titulo
 * @property string|null $fecha
 * @property string|null $tipo_test
 * @property int|null $img_n
 *
 * @property Categoriastest[] $categoriastests
 * @property Imagen $imgN
 * @property Test[] $tests
 */
class Nombretest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nombretest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['img_n'], 'integer'],
            [['titulo', 'tipo_test'], 'string', 'max' => 55],
            [['img_n'], 'exist', 'skipOnError' => true, 'targetClass' => Imagen::className(), 'targetAttribute' => ['img_n' => 'img_n']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_n' => 'Id N',
            'titulo' => 'Titulo',
            'fecha' => 'Fecha',
            'tipo_test' => 'Tipo Test',
            'img_n' => 'Img N',
        ];
    }

    /**
     * Gets query for [[Categoriastests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriastests()
    {
        return $this->hasMany(Categoriastest::className(), ['id_n' => 'id_n']);
    }

    /**
     * Gets query for [[ImgN]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImgN()
    {
        return $this->hasOne(Imagen::className(), ['img_n' => 'img_n']);
    }

    /**
     * Gets query for [[Tests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTests()
    {
        return $this->hasMany(Test::className(), ['id_n' => 'id_n']);
    }
}
