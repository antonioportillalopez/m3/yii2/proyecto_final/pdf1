<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "respuesta".
 *
 * @property int $res_n
 * @property string $letra
 * @property int $pre_n
 * @property string $txtres
 * @property int $correcta
 * @property int|null $img_n
 *
 * @property Imagen $imgN
 * @property Pregunta $preN
 */
class Respuesta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'respuesta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['letra', 'pre_n', 'txtres', 'correcta'], 'required'],
            [['pre_n', 'correcta', 'img_n'], 'integer'],
            [['txtres'], 'string'],
            [['letra'], 'string', 'max' => 1],
            [['letra', 'pre_n'], 'unique', 'targetAttribute' => ['letra', 'pre_n']],
            [['img_n'], 'exist', 'skipOnError' => true, 'targetClass' => Imagen::className(), 'targetAttribute' => ['img_n' => 'img_n']],
            [['pre_n'], 'exist', 'skipOnError' => true, 'targetClass' => Pregunta::className(), 'targetAttribute' => ['pre_n' => 'pre_n']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'res_n' => 'Res N',
            'letra' => 'Letra',
            'pre_n' => 'Pre N',
            'txtres' => 'Txtres',
            'correcta' => 'Correcta',
            'img_n' => 'Img N',
        ];
    }

    /**
     * Gets query for [[ImgN]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImgN()
    {
        return $this->hasOne(Imagen::className(), ['img_n' => 'img_n']);
    }

    /**
     * Gets query for [[PreN]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreN()
    {
        return $this->hasOne(Pregunta::className(), ['pre_n' => 'pre_n']);
    }
}
