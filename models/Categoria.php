<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categoria".
 *
 * @property int $cat_id
 * @property string|null $cat_titulo
 * @property int|null $img_n
 * @property int|null $pertenece
 *
 * @property Imagen $imgN
 * @property Categoria $pertenece0
 * @property Categoria[] $categorias
 * @property Categoriaspregunta[] $categoriaspreguntas
 * @property Categoriastest[] $categoriastests
 */
class Categoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img_n', 'pertenece'], 'integer'],
            [['cat_titulo'], 'string', 'max' => 40],
            [['img_n'], 'exist', 'skipOnError' => true, 'targetClass' => Imagen::className(), 'targetAttribute' => ['img_n' => 'img_n']],
            [['pertenece'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['pertenece' => 'cat_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cat_id' => 'Cat ID',
            'cat_titulo' => 'Cat Titulo',
            'img_n' => 'Img N',
            'pertenece' => 'Pertenece',
        ];
    }

    /**
     * Gets query for [[ImgN]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImgN()
    {
        return $this->hasOne(Imagen::className(), ['img_n' => 'img_n']);
    }

    /**
     * Gets query for [[Pertenece0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertenece0()
    {
        return $this->hasOne(Categoria::className(), ['cat_id' => 'pertenece']);
    }

    /**
     * Gets query for [[Categorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasMany(Categoria::className(), ['pertenece' => 'cat_id']);
    }

    /**
     * Gets query for [[Categoriaspreguntas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriaspreguntas()
    {
        return $this->hasMany(Categoriaspregunta::className(), ['cat_id' => 'cat_id']);
    }

    /**
     * Gets query for [[Categoriastests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriastests()
    {
        return $this->hasMany(Categoriastest::className(), ['cat_id' => 'cat_id']);
    }
}
