<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categoriaspregunta".
 *
 * @property int $id_cp
 * @property int|null $pre_n
 * @property int|null $cat_id
 *
 * @property Categoria $cat
 * @property Pregunta $preN
 */
class Categoriaspregunta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categoriaspregunta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pre_n', 'cat_id'], 'integer'],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['cat_id' => 'cat_id']],
            [['pre_n'], 'exist', 'skipOnError' => true, 'targetClass' => Pregunta::className(), 'targetAttribute' => ['pre_n' => 'pre_n']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cp' => 'Id Cp',
            'pre_n' => 'Pre N',
            'cat_id' => 'Cat ID',
        ];
    }

    /**
     * Gets query for [[Cat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(Categoria::className(), ['cat_id' => 'cat_id']);
    }

    /**
     * Gets query for [[PreN]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreN()
    {
        return $this->hasOne(Pregunta::className(), ['pre_n' => 'pre_n']);
    }
}
