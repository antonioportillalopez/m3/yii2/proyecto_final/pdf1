<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categoriastest".
 *
 * @property int $id_ct
 * @property int|null $id_n
 * @property int|null $cat_id
 *
 * @property Categoria $cat
 * @property Nombretest $n
 */
class Categoriastest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categoriastest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_n', 'cat_id'], 'integer'],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['cat_id' => 'cat_id']],
            [['id_n'], 'exist', 'skipOnError' => true, 'targetClass' => Nombretest::className(), 'targetAttribute' => ['id_n' => 'id_n']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ct' => 'Id Ct',
            'id_n' => 'Id N',
            'cat_id' => 'Cat ID',
        ];
    }

    /**
     * Gets query for [[Cat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(Categoria::className(), ['cat_id' => 'cat_id']);
    }

    /**
     * Gets query for [[N]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getN()
    {
        return $this->hasOne(Nombretest::className(), ['id_n' => 'id_n']);
    }
}
