<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "imagen".
 *
 * @property int $img_n
 * @property string $link
 * @property string|null $descripcion
 *
 * @property Categoria[] $categorias
 * @property Nombretest[] $nombretests
 * @property Pregunta[] $preguntas
 * @property Respuesta[] $respuestas
 */
class Imagen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'imagen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link'], 'required'],
            [['link', 'descripcion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'img_n' => 'Img N',
            'link' => 'Link',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Categorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasMany(Categoria::className(), ['img_n' => 'img_n']);
    }

    /**
     * Gets query for [[Nombretests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombretests()
    {
        return $this->hasMany(Nombretest::className(), ['img_n' => 'img_n']);
    }

    /**
     * Gets query for [[Preguntas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreguntas()
    {
        return $this->hasMany(Pregunta::className(), ['img_n' => 'img_n']);
    }

    /**
     * Gets query for [[Respuestas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRespuestas()
    {
        return $this->hasMany(Respuesta::className(), ['img_n' => 'img_n']);
    }
}
