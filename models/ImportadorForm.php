<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * ContactForm is the model behind the contact form.
 */
class ImportadorForm extends Model
{
    
    public $txtFile;
    public $verifyCode;
    public $rutaarchivolocal;


    /**
     * @return array the validation rules.
     */
    
    public function rules()
    {
        return [
            [['txtFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'txt'],
        ];
    }
    
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
           // 'verifyCode' => 'Verification Code',
        ];
    }

    /**
     *Importa el archivo a una carpeta de la Web
     **/
       public function subirarchivo()
    {
        if ($this->validate()) {
            $this->rutaarchivolocal='uploads/' . $this->txtFile->baseName . '.' . $this->txtFile->extension;
            $this->txtFile->saveAs($this->rutaarchivolocal);
            return true;
        } else {
            return false;
        }
    }
    
    public function abrirarchivo()
    {
        if ($this->validate()) {
            $this->txtFile->openAS($his->rutaarchivolocal);
            return true;
        } else {
            return false;
        }
    }
    
    
    
    
}
