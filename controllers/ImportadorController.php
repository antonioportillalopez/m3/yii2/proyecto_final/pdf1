<?php
namespace app\controllers;

use Yii;
use app\models\ImportadorForm;
use yii\web\Controller;
use app\models\UploadForm;
use yii\web\UploadedFile;

class ImportadorController extends Controller
{
    public function actionIndex() /* primero importar el archivo al servidor */
    {
        $modelform = new ImportadorForm(); //*instanciamos el modelo*/

        if (Yii::$app->request->isPost) { //* si das al botón devuelve un true*/
            $modelform->txtFile = UploadedFile::getInstance($modelform, 'txtFile'); //* sube el archivo */
            if ($modelform->subirarchivo()) { //*si ya está subido devuelve un true*/
                // file is uploaded successfully
                $this->previsualizar();
                return;
            }
        }

        return $this->render('examinar', ['modelform' => $modelform]); //*renderizamos */
    }
    
    public function previsualizar()
    {
        
    }
    

}
