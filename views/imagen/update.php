<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Imagen */

$this->title = 'Update Imagen: ' . $model->img_n;
$this->params['breadcrumbs'][] = ['label' => 'Imagens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->img_n, 'url' => ['view', 'id' => $model->img_n]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="imagen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
