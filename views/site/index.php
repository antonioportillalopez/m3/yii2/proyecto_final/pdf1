<?php
use yii\helpers\Html;


/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<!-- Botones de la base de datos -->

<p>
<?php
   
   echo Html::a('Nombre, fecha, tipo del test,  &raquo', ['/nombretest/index', ], ['class' => 'btn btn-default']);
   echo Html::a('Teses &raquo', ['/test/index', ], ['class' => 'btn btn-default']);
   echo Html::a('Imagenes para todo &raquo', ['/imagen/index', ], ['class' => 'btn btn-default']);
   echo Html::a('CATEGORIAS &raquo', ['/categoria/index', ], ['class' => 'btn btn-default']);
   echo Html::a('Categorias de una pregunta  &raquo', ['/categoriaspregunta/index', ], ['class' => 'btn btn-default']);
   echo Html::a('Categorias de un test  &raquo', ['/categoriastest/index', ], ['class' => 'btn btn-default']);
   echo Html::a('Preguntas &raquo', ['/pregunta/index', ], ['class' => 'btn btn-default']);
   echo Html::a('Respuestas &raquo', ['/respuesta/index', ], ['class' => 'btn btn-default']);
   
         
   
?>
</p>

<div class="jumbotron">

    
        <h1>Generador de Test</h1>

        <p class="lead"> </p>

        <p>
   <?php
    Yii::setAlias('@direcciondiagrama','http://localhost/phpmyadmin/db_designer.php?db=pdf1');
    echo Html::a("Mapa base de datos", Yii::getAlias("@direcciondiagrama"), ["class" => "btn btn-lg btn-success", "target"=>"_blank"]);
   ?>
        </p>
        
    
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>
                    <?= Html::a("IMPORTADOR", ["/importador/index"]);?>
                </h2>

                <p>Importa de un archivo de texto plano a la base de datos, test por test.</p>

                
            </div>
            <div class="col-lg-4">
                
                <h2><?= Html::a("GENERADOR", ["/generador/index"]);?></h2>

                <p>Genera test utlizando las preguntas existentes en la base de datos.</p>

            </div>
            <div class="col-lg-4">
                <h2><?= Html::a("SALIDA", ["/salida/index"]);?></h2>

                <p>Imprime a PDF los test que existen en la base de datos.</p>

            </div>
        </div>

    </div>
</div>


