<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nombretest */

$this->title = 'Update Nombretest: ' . $model->id_n;
$this->params['breadcrumbs'][] = ['label' => 'Nombretests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_n, 'url' => ['view', 'id' => $model->id_n]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nombretest-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
