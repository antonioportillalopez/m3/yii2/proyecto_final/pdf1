<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nombretest */

$this->title = 'Create Nombretest';
$this->params['breadcrumbs'][] = ['label' => 'Nombretests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nombretest-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
