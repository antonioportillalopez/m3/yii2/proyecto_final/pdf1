<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nombretests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nombretest-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Nombretest', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_n',
            'titulo',
            'fecha',
            'tipo_test',
            'img_n',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
