<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categoriastests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoriastest-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Categoriastest', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_ct',
            'id_n',
            'cat_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
