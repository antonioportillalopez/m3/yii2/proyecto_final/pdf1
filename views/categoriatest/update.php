<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categoriastest */

$this->title = 'Update Categoriastest: ' . $model->id_ct;
$this->params['breadcrumbs'][] = ['label' => 'Categoriastests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_ct, 'url' => ['view', 'id' => $model->id_ct]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="categoriastest-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
