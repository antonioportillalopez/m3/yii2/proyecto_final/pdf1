<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Categoriaspregunta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categoriaspregunta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pre_n')->textInput() ?>

    <?= $form->field($model, 'cat_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
