<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categoriaspregunta */

$this->title = 'Update Categoriaspregunta: ' . $model->id_cp;
$this->params['breadcrumbs'][] = ['label' => 'Categoriaspreguntas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_cp, 'url' => ['view', 'id' => $model->id_cp]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="categoriaspregunta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
