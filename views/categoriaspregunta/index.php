<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categoriaspreguntas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoriaspregunta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Categoriaspregunta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_cp',
            'pre_n',
            'cat_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
