<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categoriaspregunta */

$this->title = 'Create Categoriaspregunta';
$this->params['breadcrumbs'][] = ['label' => 'Categoriaspreguntas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoriaspregunta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
