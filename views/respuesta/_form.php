<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Respuesta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="respuesta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'letra')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pre_n')->textInput() ?>

    <?= $form->field($model, 'txtres')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'correcta')->textInput() ?>

    <?= $form->field($model, 'img_n')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
